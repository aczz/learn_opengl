# 学习OpenGL

## day0: 环境设定

### ubuntu

**glfw**

sudo apt install libglfw3-dev


CMake:

```
find_package(glfw3 3.3 REQUIRED)
target_link_libraries(show glfw)

#or:
target_link_libraries(show dl glfw) # without find_package(glfw3)
```

## Windows

## Mac OSX
brew install glfw

## day1: 显示窗口
手动下载glad.zip。API3.3, core模式。

拷贝教程代码到`ex1_hello_window.c`，替换std::cout为printf，纯C。

![结果](./snapshot/learn_opengl_1.png)

## day2: 显示三角形
图形渲染管线(graphics pipeline)包括两部分：3D坐标转2D坐标;2D坐标转带颜色的像素。
大多数场合只需要配置顶点(vertex)和片段(fragment)着色器就行了，几何着色器可选。

是否有这样的对应关系？
opengl->cuda
shader->kernel
GLSL->cuda C
自行编写shader->自行编写kernel

![图形渲染管线](./snapshot/learn_opengl_2_graphics_pipeline.jpg)

顶点缓冲对象VBO，可以形象化为CPU和GPU之间的“公交车”，一次运送多个顶点数据。

实在看不动，去看了theCherno的opengl视频。其实很简单的：
首先从glfw官方文档拷贝最简单的样例代码，然后使用legacy模式（5行代码）绘制三角形：
```C
         glBegin(GL_TRIANGLES);
         glVertex2f(-0.5, -0.5);
         glVertex2f( 0,  0.5);
         glVertex2f(0.5, -0.5);
         glEnd();
```

然后记得链接opengl库。对于windows，是opengl32；对于mac，是-framework OpenGL。

legacy模式对于快速debug来说挺好的。当然真正生产用的代码还是应当用core模式。


---
看网页实在搞不动。开始看视频了，台湾小哥录制的：https://www.bilibili.com/video/av24353839/?p=3

这个视频也是按照learnopengl.com的内容来做的，讲话速度比theCherno慢，不至于跟不上。视频里是在windows下做的，我是在mac osx上做。

## P3 02-Hello Window(1)
这一集能够显示一个最简单的window，也谈不上绘制窗口，就是一个黑框框。

使用glew替代了教程中的glad，据说差不多。

我基于cmake构建，配置了glfw和glew。需要的可以看我的`cmake_find_scripts`项目。

## P4 03-Hello Window(2)
这一集添加了两个功能：
1. 相应键盘事件，如果按ESC就关闭窗口;
2. 第一次真正的绘制窗口，清屏为黑板的灰绿色。

## P6 Hello Triangle
P5是在解读graphics pipeline，没有具体写代码。

P6一上来用Blender展示了三角形模型对应的模型文件.obj的内容，意在说明真正的模型文件不是直接弄好的vertices顶点数组，而是需要从模型文件转换过来的。

好吧，一开始我还以为傅老师懂很多讲的很细，但这里跟着学了Blender终于也弄出来了.obj文件后发现并无大用。关键在于明确VAO、VBO的用法：不一定真的完全理解原理，但是要知道用法：

VAO相当于VBO的容器，一个VAO可以绑定多个VBO，在教程中是只绑定了一个VBO：
![VAO相当于VBO的容器](./snapshot/VAO_VBO_relationship.png)

其次是明确，VAO、VBO相当于一个GPU缓存，用一个unsigned int来标识，相当于GPU内存地址。

然后是把shader相关的编译、链接、加载，封装到一个函数中，这样一来main函数看起来就清爽很多了。

此外，还要明确的是，当一个VBO绑定时，是需要制定绑定的buffer类型的，也就是GL_ARRAY_BUFFER。而后续对于GL_ARRAY_BUFFER类型的操作，其实就是在操作这个被绑定的VBO。

EBO这里还没有运行代码，不过看起来和VBO的用法是类似的，也是绑定到一个特定的buffer类型上：GL_ELEMENT_ARRAY_BUFFER，然后拷贝CPU上的数据到这个GPU buffer上。后续对于GL_ELEMENT_ARRAY_BUFFER类型进行操作，实际上就是在操作这个被绑定的EBO。(这个操作目前来看其实就是拷贝数据，以及unbind，其他操作还不了解）。

需要明确context的存在，然后先创建和bind一个VAO到context，再创建和bind一个VBO到VAO：
![VAO和VBO的灵魂手绘](./snapshot/vao_vbo_mr_fu.png)

