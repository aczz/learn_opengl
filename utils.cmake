#################################################
#
# Useful funtions
#
#################################################

# --[ correctly show folder structure in Visual Studio
function(assign_source_group)
    foreach(_source IN ITEMS ${ARGN})
        if (IS_ABSOLUTE "${_source}")
            file(RELATIVE_PATH _source_rel "${CMAKE_CURRENT_SOURCE_DIR}" "${_source}")
        else()
            set(_source_rel "${_source}")
        endif()
        get_filename_component(_source_path "${_source_rel}" PATH)
        string(REPLACE "/" "\\" _source_path_msvc "${_source_path}")
        source_group("${_source_path_msvc}" FILES "${_source}")
    endforeach()
endfunction(assign_source_group)

function(afq_add_executable)
	if (CMAKE_SYSTEM_NAME MATCHES "Windows" OR CMAKE_SYSTEM_NAME MATCHES "Darwin")
		foreach(_source IN ITEMS ${ARGN})
			assign_source_group(${_source})
		endforeach()
		#message("${ARGV}\n")
	endif ()
	add_executable(${ARGV})
endfunction(afq_add_executable)

function(afq_add_library)
	if (CMAKE_SYSTEM_NAME MATCHES "Windows" OR CMAKE_SYSTEM_NAME MATCHES "Darwin")
		foreach(_source IN ITEMS ${ARGN})
			assign_source_group(${_source})
		endforeach()
		#message("${ARGV}\n")
	endif ()
	add_library(${ARGV})
endfunction(afq_add_library)

# --[ useful cflags for .c files
if (CMAKE_SYSTEM_NAME MATCHES "Windows")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /we4013 /we4133 /we4716")
elseif (CMAKE_SYSTEM_NAME MATCHES "Linux" OR CMAKE_SYSTEM_NAME MATCHES "Darwin")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Werror=implicit-function-declaration -Werror=incompatible-pointer-types -Werror=return-type")
endif()